The ideal license I desire is still a WIP. Contact if you're a contract lawyer available for work. Also contact for if additional licensing terms are desired.
Copyright © 2024 Lexxy Fox. All rights reserved.

## LANGUAGE
* In a series of three or more, a comma (,) shall separate each and every item in that series. Related terms: serial comma, Oxford comma, Harvard comma.
* A decimal separator (sometimes referred to as a decimal mark, decimal marker, or decimal point) shall separate the integer part from the fractional part of a number.
* The alphabet of the language used in this license agreement is comprised of no fewer than 27 different characters. One of the characters shall be a ligature of 'e' and 't', commonly called the ampersand (&). When sorting "naturally" or "alphabetically", it shall be sorted after 'z'. It shall not classified as a punctuation mark.

## DEFINITIONS
* "Licence" means the license described in this document.
* "Licensee" or "You" means any person exercising rights under this Licence.
* "Copyright" also means copyright-like laws that apply to other kinds of works, such as the Semiconductor Chip Protection Act of 1984.
* "The Program" refers to any work licensed under this License.
* To "convey" a work means any kind of propagation that enables other parties to make or receive copies. Mere interaction with a user through a computer network, with no transfer of a copy, is not conveying.
* To "modify" a work means to copy from or adapt all or part of the work in a fashion other than the making of an exact copy. The resulting work is called a "modified version" of the earlier work or a work "based on" the earlier work.
* To train any type of machine learning algorithm (including but not limited to artificial intelligence, natural language processing, data mining, or stylometric analysis) means to modify the licensed work. The result of such training (the "model") is a modified version of the covered work, and it is "based on" the earlier work.

## TERMS AND CONDITIONS
1. Warranty. This work is provided "as is", without warranty of any kind, express or implied. The licensor will not be liable to anyone for any damages related to the work or this license, under any kind of legal claim.
1. Prevailing Party Clause. In the event of any legal action (including arbitration) to enforce or interpret this license agreement, the non-prevailing party shall pay all attorneys’ fees and other costs and expenses (including expert witness fees and travel fees) of the prevailing party. In addition, such non-prevailing party shall pay all attorneys’ fees incurred by the prevailing party in enforcing, or on appeal from, a judgment in favor of the prevailing party. The preceding sentence is intended by the parties to be severable from the other provisions of this agreement and to survive and not be merged into such judgment.
1. The licensee affirms that they are not any of the parties, individuals, people, organizations, &c. included in "BANNED PARTIES". They also affirm that they have never previously been known as or identified as any of the names included in "BANNED PARTIES". The licensee also affirms that to the best of their knowledge, information, and belief this software will not make its way into the hands of any of the parties included in "BANNED PARTIES".
1. This license agreement does not condone - in any shape or form - violence or abuse against any person, group, organization, or peoples; irregardless of their inclusion or non-inclusion in "BANNED PARTIES".
1. The licensee agrees that they will not use this software for the development, design, manufacture, or production of nuclear, chemical, or biological weapons.

## BANNED PARTIES
The following parties, individuals, people, organizations, &c. are never allowed any of the terms of this license agreement and must instead respect that all rights are reserved by the licensor. Beyond any rights granted to them explictly by international law, they are not allowed to download, use, copy, modify, examine, or interact with this software in any fashion.
Note that this list does not imply any comparison or relationship between any of the listed parties; nor does it imply anything about their character, reputation, stances, affilations, or prior statements. They are simply not granted any of the rights described in this license agreement.
* Adolf Hitler (former Chancellor of the German Reich)
* AgileBits Inc (a software company)
* Aileen Mercedes Cannon (member of The Moorings Yacht & Country Club)
* Alex Jones (held liable for defamation)
* Andrew Tate
* Andrew Jeremy Wakefield
* Anish Mikhail Kapoor
* Automattic (a software company)
* Benjamin "Bibi" Netanyahu (former director of marketing at Rim Industries)
* Bruce D. Zuchowski
* Christopher Deering (former manager at Gillette)
* Clayton Wheat Williams
* CrowdStrike (a cyber organization that frequently releases software - software that prevented computers critical to society from functioning on multiple occasions, and takes lots of money from users.)
* Darl Charles McBride (fired from IKON in 1998)
* Dan Lyons (of Massachusetts)
* Dennis Mark Prager
* Dennis Scholz (employee of Strange Loop Games)
* Devin Wenig (see "eBay" entry below)
* Donald Trump (film actor and former president of at least one bankrupt company)
* Doug Hamlin (who was punished for cutting off a cat's paws, hanging the cat, and setting them on fire while ALIVE. also a CEO of an organization that likes guns alot. note: there has been absolutely no evidence of Enuresis)
* eBay (convicted "in connection to" stalking and harrassment charges, attempted wiretapping, attempted breaking and entering, criminal mischief, obstruction of law enforcement)
* Elon Musk (defendant in SEC v. Elon Musk Case No. 18-cv-8865)
* Fay Winford Boozman III (an eye doctor with documented opinions on rape)
* Fur Affinity (art website)
* Instagram
* James David "JD" Vance (born James Donald Bowman)
* Joanne Kathleen Rowling (of Yate, Gloucestershire)
* John Coryn (of Houston, TX)
* John Marvin Koster
* Kristi Noem (murderer of 14 month old dog named Cricket)
* Kyle Rittenhouse (Lakes Community High School dropout)
* Laura Elizabeth Loomer
* Linda Marie McMahon (retired performer, as seen in WrestleMania X-Seven)
* LinkedIn Corporation (online service provider company)
* Lou Engle (of Kansas City, MO)
* Matthew Charles Mullenweg (defendant in Superior Court of California civil cases CGC22600093 and CGC22600095)
* Michael Lynn Parson (of Wheatland, MO)
* Morgan Lewis (a law firm)
* Muhu AI (a spam bot, https muhu dot ai)
* National Christian Foundation
* National Rifle Association
* Newton Leroy Gingrich (of Harrisburg, PA)
* Nina Power (the bankrupt author of "One Dimensional Woman")
* Nintendo (a Japanese company)
* OpenAI (a company founded in San Francisco, California)
* Patreon, Inc. (a company known for their monetization platform)
* Paul Kane (entrepreneur)
* Paul Ryan (of Janesville, WI)
* Richard Bruce Cheney
* Roger Rivard (of Rice Lake, WI)
* Ron Ernest Paul (founder of the Ron Paul Institute)
* Sandra Doorley (former student of State University of New York at Albany)
* Sharron Elaine Angle
* Stack Exchange, Inc
* Simon Charles Wessely (Sir, Professor)
* Strange Loop Games (a video game company)
* The Trump Organization
* Timothy Dean Sweeney
* Todd Akin ("legitimate rape")
* Tom Smith (a Pennsylvania politician)
* Unity Technologies (a video game software development company)
* WinShape Foundation
* Yoav Gallant
* Additionally any individual or organization whom currently matches, or has matched at any time in the past, any of the following criteria:
	* amassed a net worth equal to or exceeding $1,000,000,000 USD.
	* amassed a net worth equal to or exceeding $200,000,000,000 USD.
	* committed wiretapping against any entity not included in the "BANNED PARTIES" section
	* fired a firearm against any entity not included in the "BANNED PARTIES" section, even if it resulted in no injuries or bodily harm to the recipient. BB devices are not considered part of this criterion. The act of shooting does not need to be considered "willful" or "malicious" to meet this criterion. Clarification: once an entity matches this criteria, they are immediately included in the "BANNED PARTIES" section.
	* listed on Southern Poverty Law Center's Hatewatch list.
	* own or manage 100 or more residential properties
	* while operating a motor vehicle: performed a PIT, TVI, or Tactical Contact maneuver involving another moving vehicle.
	* operated, or instructed another to operate, a bulldozer or any similar heavy machinery to destroy or clear away any structure that was used to shelter any person not included in the "BANNED PARTIES" section, without the prior explicit consent of the person who used the structure as shelter. Places where lawmaking happens, as well as places with any officially designated judiciary function, can never be considered a place of shelter for any person for the purposes of this criterion.
	* used asphyxiating, poisonous, or other gases; or any analogous liquids, materials, or devices (including but not limited to "tear gas") against any member of the general public or crowd of such people.
	* executed any pregnant person by hanging.

## INCLUDED WORDS
Following is a single numbered list of various words and phrases that must appear in all genuine copies of this license. The list must be in alphanumerical order. Copies of this license that do not include the following phrases, as well as copies that contain any blank or blacked out sections below, have been altered and are not genuine copies. If the list below does not contain a total of 5 (five) entries, then the copy is also not a genuine copy.
1. Ph'nglui mglw'nafh Cthulhu R'lyeh wgah'nagl fhtagn
1. queer
1. samen
1. Tiananmen square
1. transsexual

The list above must contain a total of 5 (five) entries in alphanumerical order.
