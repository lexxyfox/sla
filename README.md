This is my work-in-progress general purpose/software license agreement I use for my projects. Ideally the license will be similar to AGPL but also: explicitly forbid implementing DRM, explicitly allow implementing anti-DRM, explicitly forbid punishing anti-DRM implementors, explicitly allow punishing DRM implementors, correctly classify LLM models as derivitives, correctly classify LLM software as a Major Component, and also explicitly ban a number of individuals and organizations. It will be a permissive open source license.

If you're a contract lawyer available for work, please contact me.
